import socket
import random
import json
from message_pb2 import Person
import sys
HOST = 'localhost'
PORT = 8080

serverType = input("J/P")

responseJson = {
    "name":"Server",
    "platform": "macOS"
}

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    print("Start")
    s.listen()
    conn, addr = s.accept()
    with conn:
        print('Connected by', addr)
        while True:
            data = conn.recv(1024)
            if not data:
                break
            dataString = str(data,"utf-8")
            if serverType == "J":
                jsonObject =  json.loads(dataString)
                print("Name " + jsonObject["name"] + " platfrom " + jsonObject["platform"])
                conn.send(bytes(json.dumps(responseJson) , 'utf-8'))
            elif serverType == "P":
                person = Person()
                person.ParseFromString(data)
                print("Name " + person.username)
                responseProto = Person()
                responseProto.username = "Server"
                responseProto.platform = "macOS"
                tmp = responseProto.SerializeToString()
                conn.send(tmp)