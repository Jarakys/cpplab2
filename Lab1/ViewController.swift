//
//  ViewController.swift
//  Lab1
//
//  Created by Kirill on 16.04.2020.
//  Copyright © 2020 Samax. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var fioTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func connectDidTap(_ sender: Any) {
        guard let fio = fioTextField.text else { return }
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "connectedVC") as! SocketViewController
        vc.data = fio
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

