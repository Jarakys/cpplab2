//
//  SocketViewController.swift
//  Lab1
//
//  Created by Kirill on 16.04.2020.
//  Copyright © 2020 Samax. All rights reserved.
//

import UIKit

class SocketViewController: UIViewController {

    var data: String?
    var isJsonSend:Bool = false
    @IBOutlet weak var uuidLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        SocketInstance.shared.delegate = self
        SocketInstance.shared.connect(port: 8080, address: "localhost")
    }
    
    @IBAction func sendDidTap(_ sender: Any) {
        if let data = data {
            let student = StudentModel(name: data, platform: "iOS")
            let studentJson = try! JSONEncoder().encode(student)
            isJsonSend = true
            SocketInstance.shared.sendMessage(data:[UInt8](studentJson))
        }
    }
    @IBAction func sendPtotoDidTap(_ sender: Any) {
        if let data = data {
            let stude = Model_Person.with({
                $0.username = data
                $0.platform = "iOS"
            })
            SocketInstance.shared.sendMessage(data: [UInt8](try! stude.serializedData()))
        }
    }
}

extension SocketViewController : SocketEvent {
    func onClose() {
        print("Close");
    }
    
    func onOpen() {
        print("Opend")
    }
    
    func onMessage(data: [UInt8]) {
        if(!isJsonSend) {
            let data = NSData(bytes: data, length: data.count)
            let person = try! Model_Person(serializedData: data as Data)
            uuidLabel.text = "Proto " + person.username
        } else {
            uuidLabel.text = String(bytes: data, encoding: .utf8)
        }
    }
    
    func onError(error: String) {
        print(error)
    }
    
    
}
