//
//  Socket.swift
//  Lab1
//
//  Created by Kirill on 16.04.2020.
//  Copyright © 2020 Samax. All rights reserved.
//

import Foundation
import SocketSwift

protocol SocketEvent: NSObject {
    func onClose()
    func onOpen()
    func onMessage(data: [UInt8])
    func onError(error: String)
}

class SocketInstance {
    var delegate: SocketEvent?
    public static let shared = SocketInstance()
    private var isConnected: Bool
    public let socket: Socket?
    
    private init() {
        isConnected = false
        socket = try? Socket(.inet, type: .stream, protocol: .tcp) // create client socket
    }
    
    public func connect(port: SocketSwift.Port, address: String) {
        guard let socket = socket else {
            delegate?.onError(error: "Error with connect to server")
            return
        }
        if let _ = try? socket.connect(port: port) {
            isConnected = true
            DispatchQueue.global(qos: .background).async { [unowned self] in
                while true {
                    var buffer = [UInt8](repeating: 0, count:1024)
                    if let count = try? socket.read(&buffer, size: 1024), count > 0 {
                        let newArr = Array(buffer[0...count-1])
                        DispatchQueue.main.async {
                            self.delegate?.onMessage(data: newArr)
                        }
                    }
                }
            }
        } else {
            delegate?.onError(error: "Error with connect to server")
        }
    }
    
    public func sendMessage(data: [UInt8]) {
        guard isConnected else {
            delegate?.onError(error: "Socket is not connected")
            return
        }
        guard let socket = socket else {
            delegate?.onError(error: "Error send data")
            return
        }
        if let _ = try? socket.write(data) {
            // Add to queue of packages ?
        } else {
            delegate?.onError(error: "Error, with send data")
        }
    }

    
}
