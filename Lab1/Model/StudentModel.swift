//
//  StudentModel.swift
//  Lab1
//
//  Created by Kirill on 16.04.2020.
//  Copyright © 2020 Samax. All rights reserved.
//

import Foundation

struct StudentModel : Codable {
    let name: String
    let platform: String
}
